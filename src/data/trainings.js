export default [
  {
    name: 'flash',
    rounds: [
      {
        id: 1,
        duration: 10,
        name: 'sautiller avec garde de boxe'
      },
      {
        id: 2,
        duration: 15,
        name: 'rafale de coups de poings directs'
      },
      {
        id: 3,
        duration: 30,
        name: 'squats avec coups de pieds latéraux'
      },
      {
        id: 4,
        duration: 15,
        name: 'esquives rotatives'
      },
      {
        id: 5,
        duration: 15,
        name: 'burpees'
      },
      {
        id: 6,
        duration: 30,
        name: 'gainage en remontant sur les bras'
      },
      {
        id: 7,
        duration: 30,
        name: 'pompes'
      }
    ]
  },
  {
    name: '6 minutes',
    rounds: [
      {
        id: 1,
        duration: 60,
        name: 'meditation'
      },
      {
        id: 2,
        duration: 60,
        name: 'affirmations'
      },
      {
        id: 3,
        duration: 60,
        name: 'visualization'
      },
      {
        id: 4,
        duration: 60,
        name: 'write things I am grateful, proud of and results I am committed to create today'
      },
      {
        id: 5,
        duration: 60,
        name: 'read self-help book'
      },
      {
        id: 6,
        duration: 60,
        name: 'stand up and move my body fast (run, jump, push-ups...)'
      }
    ]
  },
  {
    name: 'Cohérence cardiaque 365',
    rounds: [
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      }
    ]
  }
]
