class TimeReader {
  read (time) {
    const timeParts = time.replace(/[,.]/g, ':').split(':').map((part) => parseInt(part))

    switch (timeParts.length) {
      case 1:
        return timeParts[0]
      case 2:
        return timeParts[0] * 60 + timeParts[1]
      case 3:
        return timeParts[0] * 60 * 60 + timeParts[1] * 60 + timeParts[2]
    }

    throw new Error('no time found.')
  }
}

export default TimeReader
