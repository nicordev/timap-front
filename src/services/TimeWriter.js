class TimeWriter {
  write (seconds) {
    const hoursCount = Math.floor(seconds / 3600)
    const minutesCount = Math.floor((seconds - hoursCount * 3600) / 60)
    const secondsCount = (seconds % 60)

    return [
      hoursCount,
      minutesCount,
      secondsCount
    ].map((part) => part.toFixed(0).padStart(2, '0')).join(':')
  }
}

export default TimeWriter
